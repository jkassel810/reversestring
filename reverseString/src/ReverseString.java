/**
 * Created by jkass on 11/26/2016.
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class ReverseString {

    public static String reverseString (String myString){

        char[] myCharArray = myString.toCharArray();
        String myNewString = "";
        for (int i=myCharArray.length-1; i>=0; i--){
            myNewString += String.valueOf(myCharArray[i]);
        }
        return myNewString;

    }

    public static void main(String[] args) throws IOException {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Input a String to Reverse (type 'quit' to exit)>");
      String inString = "";
      while(true) {
          inString = br.readLine();
          if (inString.toLowerCase().equals("quit")){
              System.out.println("quitting...");
              break;
          }
          System.out.println("Reversing String: " + inString);
          String myResult = reverseString(inString);
          System.out.println("Result: " + myResult);
          System.out.print("Try Another (type 'quit' to exit) >");

      }
    }

}
